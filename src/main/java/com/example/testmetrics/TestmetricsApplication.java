package com.example.testmetrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestmetricsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestmetricsApplication.class, args);
    }
}
