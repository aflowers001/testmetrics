package com.example.testmetrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class MyController {
    @Autowired
    MetricsService metricsService;

    @GetMapping()
    public void incrementStuff() {
        metricsService.increment("my.metric", "value", "666");
        metricsService.increment("my.other");
    }
}
