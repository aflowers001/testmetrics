package com.example.testmetrics;

import java.util.List;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

@Component
public class MetricsService {
    private final MeterRegistry meterRegistry;

    public MetricsService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        initialiseCustomMetrics();
    }

    private void initialiseCustomMetrics() {
        List.of("my.other").stream()
                .map(s -> s)
                .forEach(meterRegistry::counter);
        List.of("my.metric").stream()
                .map(s -> s)
//                .forEach(s -> meterRegistry.counter(s, "value", ""));
                .forEach(s -> meterRegistry.counter(s));
    }

    public void increment(String metricName, String... tags) {
        meterRegistry.counter(metricName, tags).increment(1);
    }
}
